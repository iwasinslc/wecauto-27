<?php
namespace App\Http\Controllers\Telegram\account_bot\Topup;

use App\Helpers\Constants;
use App\Http\Controllers\Controller;
use App\Models\AutoCreateDeposit;
use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Rate;
use App\Models\Telegram\TelegramBotEvents;
use App\Models\Telegram\TelegramBotMessages;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramBotScopes;
use App\Models\Telegram\TelegramUsers;
use App\Models\Telegram\TelegramWebhooks;
use App\Models\Wallet;
use App\Modules\Messangers\TelegramModule;

class AmountController extends Controller
{
    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function index(TelegramWebhooks $webhook,
                          TelegramBots $bot,
                          TelegramBotScopes $scope,
                          TelegramUsers $telegramUser,
                          TelegramBotEvents $event)
    {
        TelegramModule::setLanguageLocale($telegramUser->language);

//        $scope = TelegramBotScopes::where('command', 'create_depo_wallet')
//            ->where('bot_keyword', $bot->keyword)
//            ->first();

        preg_match('/topup\_amount '.Constants::UUID_REGEX.'/', $event->text, $data);


        if (isset($data[1]))
        {
            $rate_id = $data[1];
        }
        else {
            return response('ok');
        }


        $user = $telegramUser->user;


        $topup_data =  cache()->get('topup_data'.$user->id);

        if ($topup_data==null)
        {
            $error = __('Time to invest has expired');
            $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $error);
            return response('ok');
        }


        $topup_data['rate_id'] = $rate_id;

        cache()->put('topup_data'.$user->id, $topup_data , 30 );

        $message = view('telegram.account_bot.topup.amount', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'event'        => $event,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }


        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id,
                $message,
                'HTML',
                true,
                false,
                null,
                null,
                $scope,
                'inline_keyboard');
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }




        return response('ok');
    }


    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function checkAndProcessAnswer(
        TelegramWebhooks $webhook,
        TelegramBots $bot,
        TelegramBotScopes $scope,
        TelegramUsers $telegramUser,
        TelegramBotEvents $event,
        TelegramBotMessages $userMessage,
        TelegramBotMessages $botRequestMessage
    ) {

        TelegramModule::setLanguageLocale($telegramUser->language);
        /*
         * Validate inputs
         */
        $validator = \Validator::make([
            'amount' => $userMessage->message
        ], [
            'amount' => ['numeric', 'min:0.0000001'],
        ]);

        $amount = $userMessage->message;

        if ($validator->fails()) {
            $error = __('Amount should be numeric');
            $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $error);
            return response('ok');
        }

        /*
         * Get and save info
         */
        $user = $telegramUser->user;

        if (null === $user) {
            throw new \Exception('User can not be found.');
        }

        $topup_data =  cache()->get('topup_data'.$user->id);


        if ($topup_data==null)
        {
            $error = __('Time to invest has expired');
            $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $error);
            return response('ok');
        }

        $rate = Rate::find($topup_data['rate_id']);

        if (null == $rate) {
            $error = __('Rate is not found');
            $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $error);
            return response('ok');
        }

        if ($amount < $rate->min || $amount > $rate->max) {
            $error = __('Amount not in range with rate');
            $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $error);
            return response('ok');
        }

        $topup_data['amount'] = $amount;

        cache()->put('topup_data'.$user->id, $topup_data , 30 );

        /*
         * Answer
         */
        $this->userAnswerSuccess($webhook, $bot, $scope, $telegramUser, $event, $userMessage, $botRequestMessage, $amount);

        return response('ok');
    }

    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function userAnswerSuccess(
        TelegramWebhooks $webhook,
        TelegramBots $bot,
        TelegramBotScopes $scope,
        TelegramUsers $telegramUser,
        TelegramBotEvents $event,
        TelegramBotMessages $userMessage,
        TelegramBotMessages $botRequestMessage,
        float $amount
    ) {
        TelegramModule::setLanguageLocale($telegramUser->language);


        $user = $telegramUser->user;

        if (null === $user) {
            throw new \Exception('User can not be found.');
        }


        $topup_data =  cache()->get('topup_data'.$user->id);


        if ($topup_data==null)
        {
            $error = __('Time to invest has expired');
            $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $error);
            return response('ok');
        }

        $paymentSystem = PaymentSystem::find($topup_data['payment_system_id']);

        if (null === $paymentSystem) {
            throw new \Exception('PS can not be found.');
        }

        $currency = Currency::find($topup_data['currency_id']);

        if (null === $currency) {
            throw new \Exception('Currency can not be found.');
        }

        $psMinimumTopupArray = @json_decode($paymentSystem->minimum_topup, true);
        $psMinimumTopup      = isset($psMinimumTopupArray[$currency->code])
            ? $psMinimumTopupArray[$currency->code]
            : 0;

        if ($amount < $psMinimumTopup) {
            $error = __('Minimum balance recharge is').' '.$psMinimumTopup.$currency->symbol;
            $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $error);
            return response('ok');
        }

        $wallet = $user->wallets()
            ->where('currency_id', $currency->id)
            ->first();

        if (null == $wallet) {
            $wallet = Wallet::newWallet($user, $currency, $paymentSystem);
        }

        $rate = Rate::find($topup_data['rate_id']);

        if (null === $rate) {
            throw new \Exception('Rate can not be found.');
        }

        AutoCreateDeposit::create([
            'wallet_id' => $wallet->id,
            'rate_id' => $rate->id,
            'amount' => $amount,
            'user_id' => $user->id,
        ]);

        $message = view('telegram.account_bot.topup.user_answer_success', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'userMessage'  => $userMessage,
            'link'         => route('telegram.topup', [
                'user_id'           => $user->id,
                'wallet_id'         => $wallet->id,
                'payment_system_id' => $paymentSystem->id,
                'currency_id'       => $currency->id,
                'rate_id'           => $rate->id,
                'amount'            => $amount,
            ]),
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id,
                $message,
                'HTML',
                true,
                false,
                null,
                null,
                $scope,
                'inline_keyboard');
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }

        TelegramBotMessages::closeUserScopes($event, $bot);

    }

    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function validationFailed(
        TelegramWebhooks $webhook,
        TelegramBots $bot,
        TelegramBotScopes $scope,
        TelegramUsers $telegramUser,
        TelegramBotEvents $event,
        string $error
    ) {
        TelegramModule::setLanguageLocale($telegramUser->language);
        $message = view('telegram.account_bot.topup.error', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'error' => $error
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id, $message, 'HTML', true);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }
    }
}