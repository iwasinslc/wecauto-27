<?php


namespace App\Http\Controllers\Api;


class Controller
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="WebAuto API Documentation",
     *      description="All requests must include parameter: `Accept` in the `Headers` to retrieve all responces in `application/json` format"
     * )
     *
     * @OA\Server(
     *      url="http://127.0.0.1:8005/api/v1",
     *      description="Demo API Server"
     * )

     *
     * @OA\Tag(
     *     name="Users",
     *     description="API Endpoints of Users"
     * )
     * @OA\Tag(
     *     name="Transactions",
     *     description="API Endpoints of Transactions"
     * )
     * @OA\Tag(
     *     name="Withdrawals",
     *     description="API Endpoints of Withdrawals"
     * )
     * @OA\Tag(
     *     name="Deposits",
     *     description="API Endpoints of Deposits"
     * )
     *  @OA\Tag(
     *     name="Orders",
     *     description="API Endpoints of Orders"
     * )
     * @OA\Tag(
     *     name="Trades",
     *     description="API Endpoints of Trades"
     * )
     */
    // Set Accept type to each header
    /** @OA\Parameter(
     *     name="Accept",
     *     description="Type of the response",
     *     required=true,
     *     in="header",
     *     example="application/json",
     *     @OA\Schema(
     *        type="string"
     *     )
     * )
     */
}