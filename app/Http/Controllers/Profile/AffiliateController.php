<?php
namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\Licences;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\User;
use DB;
use Yajra\DataTables\DataTables;

class AffiliateController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($lvl = 1)
    {

        if ($lvl>10)
        {
            back();
        }

        $partner_type = TransactionType::getByName('partner');
        $licence_type = TransactionType::getByName('buy_license');

        $total_refs = user()->transactions()->where('type_id', $partner_type->id)->sum('amount');
        $level_refs = user()->transactions()->where('type_id', $partner_type->id)->where('result', 'like' ,(string)$lvl)->sum('amount');

        $total_lic = user()->referralsTransactions()->wherePivot('line','<=', 10)->where('type_id', $licence_type->id)->sum('amount');
        $level_lic = user()->referralsTransactions()->wherePivot('line',$lvl)->where('type_id', $licence_type->id)->sum('amount');

        $licences = Licences::orderBy('buy_amount')->get();

        $stat =  user()->referralsTransactions()->selectRaw('source')
            ->wherePivot('line',$lvl)->where('type_id', $licence_type->id)
//            ->groupBy(['source'])
            ->orderBy('source')
            ->get();

        $lic_count = [];

        foreach ($stat as $item) {
            if (!isset($lic_count[$item->source]))
            {
                $lic_count[$item->source] = 0;
            }

            $lic_count[$item->source]++;
        }



//




        return view('profile.affiliate', [
            'lvl'=>$lvl,
            'total_refs'=>$total_refs,
            'level_refs'=>$level_refs,
            'total_lic'=>$total_lic,
            'level_lic'=>$level_lic,
            'lic_count'=>$lic_count,
            'licences' => $licences,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function datatable()
    {
        $lvl = 1;
        if (request()->has('lvl'))
        {
            $lvl = request()->lvl;
            if ($lvl>4)
            {
                back();
            }
        }

        $refferals = user()->referrals()->wherePivot('line', $lvl)->withPivot('line')->with(['rank'])->with('licence');

        return DataTables::of($refferals)
//            ->editColumn('condition', function ($deposit) {
//                return __($deposit->condition);
//            })

            ->addColumn('licence_status', function (User $user) {
                return $user->licence!==null&&$user->activeLicence() ? $user->licence->price : __('empty');
            })


            ->addColumn('active', function (User $user) {
                return $user->deposits()->where('active', true)->exists() ? __('yes') : __('no');
            })
//            ->editColumn('closing_at', function ($deposit) {
//                return Carbon::parse($deposit->created_at)->addDays($deposit->duration)->toDateString();
//            })
            ->make(true);
    }
}
