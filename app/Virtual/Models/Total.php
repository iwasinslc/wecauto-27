<?php


namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Total",
 *     description="User total information",
 *     @OA\Xml(
 *         name="Total"
 *     )
 * )
 */
class Total
{
    /**
     * @OA\Property(
     *     title="Withdraws",
     *     description="Total successful withdraws",
     *     format="int64",
     *     example=100
     * )
     *
     * @var integer
     */
    public $withdraws;

    /**
     * @OA\Property(
     *     title="Deposited",
     *     description="Total successful deposited",
     *     format="int64",
     *     example=100
     * )
     *
     * @var integer
     */
    public $deposited;

    /**
     * @OA\Property(
     *     title="Transfers",
     *     description="Total successful send transfers",
     *     format="int64",
     *     example=100
     * )
     *
     * @var integer
     */
    public $transfers_send;

    /**
     * @OA\Property(
     *     title="Transfers",
     *     description="Total successful recieved transfers",
     *     format="int64",
     *     example=100
     * )
     *
     * @var integer
     */
    public $transfers_received;

    /**
     * @OA\Property(
     *     title="Earned",
     *     description="Total amount earned",
     *     format="int64",
     *     example=100
     * )
     *
     * @var integer
     */
    public $earned;

    /**
     * @OA\Property(
     *     title="Partner",
     *     description="Total amount of the bonuses recieved for partners",
     *     format="int64",
     *     example=100
     * )
     *
     * @var integer
     */
    public $partner;

    /**
     * @OA\Property(
     *     title="Bonuses",
     *     description="Total amount of the user's bonuses",
     *     format="int64",
     *     example=1000
     * )
     *
     * @var integer
     */
    public $bonus;

    /**
     * @OA\Property(
     *     title="Charity",
     *     description="Total amount of the charity",
     *     format="int64",
     *     example=10
     * )
     *
     * @var integer
     */
    public $charity;
}