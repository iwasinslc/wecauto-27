<?php


namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Transaction",
 *     description="Transaction information"
 * )
 */
class Transaction
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="Transaction ID",
     *     example="b5d00f20-ff1d-11ea-8e8c-211d59c5f58c"
     * )
     *
     * @var string
     */
    private $id;

    /**
     * @OA\Property(
     *     title="Type",
     *     description="Transaction type",
     *     example="send_transfer"
     * )
     *
     * @var string
     */
    private $type;

    /**
     * @OA\Property(
     *     title="Value",
     *     description="Transaction amount",
     *     example=1000.00
     * )
     *
     * @var float
     */

    public $value;

    /**
     * @OA\Property(
     *     title="Currency",
     *     description="Currency code of the transaction amount",
     *     example="WEC"
     * )
     *
     * @var string
     */
    public $currency;

    /**
     * @OA\Property(
     *     title="Approved",
     *     description="Show if transaction is approved",
     *     example=true
     * )
     *
     * @var boolean
     */
    public $approved;

    /**
     * @OA\Property(
     *     title="Batch id",
     *     description="Batch id of the withdrawal"
     * )
     *
     * @var string
     */
    public $batch_id;

    /**
     * @OA\Property(
     *     title="Order Id",
     *     description="Order id for transaction"
     * )
     *
     * @var integer
     */
    public $order_id;

    /**
     * @OA\Property(
     *     property="source",
     *     title="Source",
     *     description="Different additional information",
     *     type="string"
     * )
     * @var string
     */
    public $source;

    /**
     * @OA\Property(
     *     property="result",
     *     title="Result",
     *     description="Result information for transactions",
     *     type="string"
     * )
     * @var string
     */
    public $result;

    /**
     * @OA\Property(
     *     title="Created At",
     *     description="Date of the creating transaction",
     *     example="2020-01-01 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    public $created_at;
}