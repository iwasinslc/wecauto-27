<?php
namespace App\Jobs;

use App\Models\Deposit;
use App\Models\DepositQueue;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

/**
 * Class HandleRankJob
 * @package App\Jobs
 */
class HandleRankJob implements ShouldQueue
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var User $user */
    public $user;


    /**
     * AccrueDeposit constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user      = $user;
    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $this->user->checkRank();
    }
}
