<?php
namespace App\Rules;

use App\Models\Deposit;
use App\Models\Licences;
use Illuminate\Contracts\Validation\Rule;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RuleEnoughBalance
 * @package App\Rules
 */
class RuleFSTEnoughBalance implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $wallet = getUserWallet('FST');

        if ($wallet===null) {
            return false;
        }

        /**
         * @var Deposit $deposit
         */
        $deposit = Deposit::find(request()->deposit_id);

        // Get number of executed days
        $value = $deposit->maxNumberDays() - $value;

        if ($deposit===null) {
            return false;
        }

        $amount = $value*$deposit->dayFst(true);

        return $wallet ? $wallet->balance >= $amount : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.enough_balance');
    }
}
