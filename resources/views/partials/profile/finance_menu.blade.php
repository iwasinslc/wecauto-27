<ul class="section-tabs__nav">
    <li class="{{ (Route::is('profile.topup') ? 'active' : '') }}"><a href="{{route('profile.topup')}}">{{__('Top up')}}</a>
    </li>
    <li class="{{ (Route::is('profile.withdraw') ? 'active' : '') }}"><a href="{{route('profile.withdraw')}}">{{__('Withdraw')}}</a>
    </li>
    <li  class="{{ (Route::is('profile.transfer') ? 'active' : '') }}"><a href="{{route('profile.transfer')}}">{{__('Transfer to user')}}</a>
    </li>

{{--    <li  class="{{ (Route::is('profile.acc_withdraw') ? 'active' : '') }}"><a href="{{route('profile.acc_withdraw')}}">{{__('Transfer ACC to WebtokenProfit')}}</a>--}}
{{--    </li>--}}



</ul>