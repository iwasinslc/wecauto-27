@extends('layouts.profile')
@section('title', 'Мои заявки')

@section('content')
    <div class="cashback">
        <!---->
        <section>
            <div class="container">
                <ul class="tabs-navigation">
                    <li><a href="{{ route('profile.cashback.conditions') }}">Как получить Cash Back?</a>
                    </li>
                    <li><a href="{{ route('profile.cashback.create') }}">Подать заявку</a>
                    </li>
                    <li class="is-active"><a href="{{ route('profile.cashback.index') }}">Мои заявки</a>
                    </li>
                </ul>
            </div>
        </section>
        <!---->
        <section>
            <div class="container">
                <h2 class="lk-title">Мои заявки
                </h2>
                <div class="claim-details content-block">
                    <div class="claim">
                        {{--<p class="claim__id">0001
                        </p>--}}
                        <p class="claim__date">{{$cashbackRequest->created_at}}
                        </p>
                        @if($cashbackRequest->approved === null)
                            <p class="status">
                                <svg class="svg-icon">
                                    <use href="assets/icons/sprite.svg#icon-hourglass"></use>
                                </svg><span>На рассмотрении</span>
                            </p>
                        @elseif($cashbackRequest->approved)
                            <p class="status status--gray">
                                <svg class="svg-icon">
                                    <use href="assets/icons/sprite.svg#icon-check"></use>
                                </svg><span>Одобрена</span>
                            </p>
                        @else
                            <p class="status status--warning">
                              <svg class="svg-icon">
                                <use href="assets/icons/sprite.svg#icon-uncheck"></use>
                              </svg><span>Отклонена</span>
                            </p>
                        @endif
                    </div>
                    <div class="claim-details__row">
                        <div class="claim-details__col">
                            <h6 class="claim-details__subtitle">Документы (свидетельство о регистрации ТС, договор купли-продажи ТС):
                            </h6>
                            <ul class="file-list">
                                @foreach ($documents as $document)
                                <li>
                                    <a class="file-item" href="{{ $document['url'] }}" download="download">{{ $document['name'] }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="claim-details__col">
                            <h6 class="claim-details__subtitle">Видео:
                            </h6><a href="{{ $cashbackRequest->video_link }}" target="_blank">{{ $cashbackRequest->video_link }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!---->
        @if(!empty($cashbackRequest->result))
        <section>
            <div class="container">
                <h2 class="lk-title">Результат рассмотрения
                </h2>
                <div class="content-block">
                    <div class="typography">
                        Ответ: {{ $cashbackRequest->result }}
                    </div>
                </div>
            </div>
        </section>
        @endif
    </div>
@endsection
