<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMaxRankToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('users', 'max_rank')) {
            Schema::table('users', function (Blueprint $table) {
                $table->tinyInteger('max_rank')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'max_rank')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('max_rank');
            });
        }
    }
}
