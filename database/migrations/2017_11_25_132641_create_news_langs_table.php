<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_langs', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('news_id');
            $table->string('lang_id');
            $table->boolean('show')->default(false);
            $table->string('title')->nullable();
            $table->text('teaser')->nullable();
            $table->text('text')->nullable();
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_langs');
    }
}
